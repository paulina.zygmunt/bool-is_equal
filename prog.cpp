#include <iostream>
#include <cmath>

using namespace std;

bool isEqual(float a, float b, float eps)
{
//	float x = a - b;
//	float  wb = abs(x);
//	if(wb < eps)
//		return true;
//	else
//		return false;

	return abs(a - b) < eps;
}

int main()
{
	float a, b, eps;

//	eps = 0.01; //epsilon
	cin >> a;
	cin >> b;


	if( isEqual(a, b, eps) == true ){
		std::cout << "The numbers are equal." << std::endl;
	}
	else if( isEqual(a, b, eps) == false){
		std::cout << "The numbers are different." << std::endl;
	}
	else{
		std::cout << "The numbers are equal." << std::endl;
	}

	return 0;
}

